use ndarray::Array2;

use crate::cell;

pub struct World {
    pub cells: Array2<cell::Cell>,
}

pub fn init(height: usize, width: usize) -> World {
    let cells: Array2<cell::Cell>;
    cells = Array2::from_elem((height, width), cell::Cell::Empty);
    return World { cells };
}

impl World {
    fn size(&self) -> (usize, usize) {
        let a: &[usize] = self.cells.shape();
        // guranteed two dimensions of Array2
        return (a[0], a[1]);
    }

    fn neighborhood(&self, x: usize, y: usize) -> Vec<cell::Cell> {
        let (w, h) = self.size();
        let mut arr: Vec<cell::Cell> = vec![];
        for i in -1i32..=1 {
            for j in -1i32..=1 {
                let cx = (x as i32) + i;
                let cy = (y as i32) + j;
                if (i != 0 || j != 0) && (0 <= cx && cx < w as i32) && (0 <= cy && cy < h as i32) {
                    arr.push(self.cells[[cx as usize, cy as usize]]);
                }
            }
        }
        return arr;
    }

    fn fold_neighborhood<T, F>(&self, x: usize, y: usize, init: T, f: F) -> T
    where
        F: Fn(T, &cell::Cell) -> T,
    {
        let n = self.neighborhood(x, y);
        return n.iter().fold(init, f);
    }
}

pub fn step(world: &World) -> World {
    let (w, h) = world.size();
    let mut cells: Array2<cell::Cell> = Array2::from_elem((w, h), cell::Cell::Empty);

    let (w, h) = world.size();
    for x in 0..w {
        for y in 0..h {
            let cell = world.cells[[x, y]];
            let ct = world
                .neighborhood(x, y)
                .iter()
                .filter(|&c| *c == cell::Cell::Full)
                .count();
            //
            // if x == 7 && y == 6 {
            //     println!("printing: ");
            //     for x in world.neighborhood(7, 6).iter() {
            //         println!("neighbor {}", x.to_string());
            //     }
            // }

            // if ct != 0 {
            //     println!("[{}, {}]: {}", x, y, ct);
            // }
            // Game of Life rules
            let alive = cell == cell::Cell::Full;
            let res_alive = alive && (ct >= 2 && ct <= 3) || (!alive && ct == 3);
            cells[[x, y]] = if res_alive { cell::Cell::Full } else { cell::Cell::Empty };
        }
    }
    World { cells }
}

pub fn print_world(world: &World) {
    let (w, h): (usize, usize) = world.size();
    for i in 0..w {
        for j in 0..h {
            let c: char = world.cells[[i, j]].char_of_cell();
            print!("{}", c);
        }
        println!()
    }
}
