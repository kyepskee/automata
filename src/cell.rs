use std::string::String;

#[derive(Clone, Copy, PartialEq)]
pub enum Cell {
    Empty,
    Full,
}

impl Cell {
    pub fn char_of_cell(&self) -> char {
        match *self {
            Cell::Empty => '_',
            Cell::Full => 'x',
        }
    }
    
    pub fn flip(&mut self) {
        let n: Cell = match self {
            Cell::Empty => Cell::Full,
            Cell::Full => Cell::Empty,
        };
        *self = n
    }
}

impl ToString for Cell {
    fn to_string(&self) -> String {
        match *self {
            Cell::Empty => "_".to_string(),
            Cell::Full => "#".to_string()
        }
    }
}
