mod cell;
mod world;

use std::io;
// use std::{thread, time::Duration};
// use std::sync::{Arc, atomic::AtomicBool};
use std::os::unix::net::UnixStream;
use std::io::{Read, Write};

use signal_hook::{low_level::pipe, consts::signal::SIGWINCH};
use termion;
use tokio::sync::oneshot;
// use tokio;

// use futures::{
//     s
// };

fn main() -> io::Result<()> {
    let (mut read, write) = UnixStream::pair()?;
    pipe::register(SIGWINCH, write)?;
    let mut buf: [u8; 1] = [0];
    
    let w: &mut world::World = &mut world::init(20, 20);
    let arr = [(6, 6), (7, 7), (8, 7), (8, 6), (8, 5)];
    for (x, y) in arr.iter() {
        w.cells[[*x, *y]] = cell::Cell::Full;
    }
    let mut stdout = io::stdout();
    thread::spawn(|| {
        loop {
            read.read_exact(&mut buf)?;
            let (x, y) = termion::terminal_size().unwrap();
            writeln!(stdout, "{}", termion::clear::All)?;
            for i in 0..y {
                writeln!(stdout);
                for j in 0..=x {
                    if j == 0 {
                        write!(stdout, "-");   
                    } else {
                        write!(stdout, "x");
                    }
                }
            }
        }
    });
    // let mut i = 0;
    // println!("{}", i);
    // world::print_world(w);
    // *w = world::step(w);
    // thread::sleep(Duration::from_millis(70));
    // i += 1;
    #[allow(unreachable_code)]
    Ok(())
}
